from django.urls import path
from .views import ListAllUser


urlpatterns = [
    path('users/', ListAllUser.as_view(), name="all-user")
]
# Pull base image
FROM python:3

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV APP_HOME /app

# Set work directory
RUN mkdir ${APP_HOME}
WORKDIR ${APP_HOME}

# Install dependencies
RUN pip install pipenv
COPY Pipfile Pipfile.lock ${APP_HOME}/
RUN pipenv install --system

# Copy project
COPY . ${APP_HOME}/

# Run start.sh
EXPOSE 8000
RUN chmod +x start.sh
ENTRYPOINT ["/app/start.sh"]